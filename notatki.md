## GIT

cd ..
git clone https://bitbucket.org/ev45ive/pentacomp-react-adv.git pentacomp-react-adv
cd pentacomp-react-adv
git pull  --set-upstream  origin master
npm i
npm run storybook

## Git update
git stash -u
git pull 

## Instalacje
node -v
v14.15.4

npm -v
6.14.10

code -v
1.40.1

git --version
version 2.31.1.windows.1

chrome

https://create-react-app.dev/docs/adding-typescript/

npx create-react-app moj-projekt --template typescript

npx create-react-app . --template typescript --use-npm

## Extensions

https://marketplace.visualstudio.com/items?itemName=dsznajder.es7-react-js-snippets

https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode

https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint

https://marketplace.visualstudio.com/items?itemName=quicktype.quicktype

## Custom templates

https://create-react-app.dev/docs/custom-templates

## Storybook

npx sb init
npm run storybook

## bootstrap css

npm i bootstrap

## Playlist

mkdir -p src/core/components
mkdir -p src/core/model
mkdir -p src/core/services

mkdir -p src/search/containers
mkdir -p src/search/components

mkdir -p src/playlists/containers
touch src/playlists/containers/PlaylistsView.tsx
touch src/playlists/containers/PlaylistsView.test.tsx
touch src/playlists/containers/PlaylistsView.stories.tsx

mkdir -p src/playlists/containers
mkdir -p src/playlists/components
touch src/playlists/components/PlaylistsDetails.tsx
touch src/playlists/components/PlaylistsDetails.test.tsx
touch src/playlists/components/PlaylistsDetails.stories.tsx
touch src/playlists/components/PlaylistsList.tsx
touch src/playlists/components/PlaylistsList.test.tsx
touch src/playlists/components/PlaylistsList.stories.tsx
touch src/playlists/components/PlaylistForm.tsx
touch src/playlists/components/PlaylistForm.test.tsx
touch src/playlists/components/PlaylistForm.stories.tsx

## Mock Service Worker

https://storybook.js.org/addons/msw-storybook-addon

npm install msw-storybook-addon

<!-- Generate worker.js -->
px msw init public/
Initializing the Mock Service Worker at "C:\Projects\sages\pentacomp-react-adv\public"...

Service Worker successfully created!
C:\Projects\sages\pentacomp-react-adv\public\mockServiceWorker.js

Continue by creating a mocking definition module in your application:     

https://mswjs.io/docs/getting-started/mocks

INFO In order to ease the future updates to the worker script,
we recommend saving the path to the worker directory in your package.json.
        
? Do you wish to save "public" as the worker directory? Yes
Writing "msw.workerDirectory" to "C:\Projects\sages\pentacomp-react-adv\package.json"...