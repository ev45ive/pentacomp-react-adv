import React, { useContext } from 'react'
import { UserContext } from '../contexts/UserContext'

interface Props {

}

export const NavBar = (props: Props) => {
  const { user, login, logout } = useContext(UserContext)

  return (
    <div>

      <h1 className="diplay-3">Music App</h1>
      {
        user ? <button className="btn btn-default" onClick={logout}>Logout</button>
          :
          <button className="btn btn-default" onClick={login}>Login</button>
      }

    </div>
  )
}
