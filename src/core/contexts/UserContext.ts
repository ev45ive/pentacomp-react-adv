import React from "react";
import { UserProfile } from "./UserProvider";

interface UserContext {
  user: UserProfile | null
  login: () => void;
  logout: () => void;
}

export const UserContext = React.createContext<UserContext>({
  user: null
} as UserContext)