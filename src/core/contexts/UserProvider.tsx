

import React, { useState } from 'react'
import { UserContext } from './UserContext'


export interface UserProfile {
  id: string, display_name: string
}

interface Props {
  children: React.ReactNode
}

export const UserProvider = ({ children }: Props) => {

  const [user, setUser] = useState<UserProfile | null>(null)

  const login = () => setUser({
    display_name: 'Placki', id: '213'
  })
  const logout = () => setUser(null)

  return (
    <UserContext.Provider value={{
      user, login, logout
    }}>{children}</UserContext.Provider>
  )
}

