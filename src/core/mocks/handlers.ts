import { rest } from 'msw'
import { mockPlaylists } from '../../playlists/containers/mockPlaylists'


export const handlers = [

  rest.get('/playlists', (req, res, ctx) => {
    // Check if the user is authenticated in this session
    return res(
      ctx.status(200),
      ctx.json(mockPlaylists),
    )
  }),

  rest.get('/playlists/:id', (req, res, ctx) => {
    // Check if the user is authenticated in this session
    return res(
      ctx.status(200),
      ctx.json(mockPlaylists.find(p => p.id === req.params.id)),
    )
  })
]