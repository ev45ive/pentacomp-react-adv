import React from 'react'
import { Playlist } from '../../core/model/Playlist'

interface Props {
  /**
   * List of user playlsits
   */
  playlists: Playlist[],
  /**
   * Currently selected playlist is
   */
  selectedId?: string
  /**
   * Selected playlist event
   * @param id {Playlist.id}
   */
  onSelect(id: Playlist['id']): void
}

export const PlaylistsList = ({ playlists, selectedId, onSelect }: Props) => {
  return (
    <div>
      <ul className="list-group">
        {playlists.length === 0 &&
          <li className="list-group-item text-center text-muted">No playlists</li>}

        {playlists.map((playlist, index) =>
          <li className={"list-group-item" + (selectedId === playlist.id ? ' active' : '')} aria-label="playlist-item" key={playlist.id}
            onClick={() => {
              onSelect(playlist.id)
            }}>
            {index + 1}. {playlist.name}
          </li>
        )}
      </ul>
    </div>
  )
}
