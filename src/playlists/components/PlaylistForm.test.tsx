import { render, screen } from '@testing-library/react'
import { Playlist } from '../../core/model/Playlist'
import { mockPlaylists } from '../containers/mockPlaylists'
import { PlaylistForm } from './PlaylistForm'

describe('PlaylistForm', () => {

  const setup = ({
    playlist = undefined as unknown as Playlist
  }) => {
    render(<PlaylistForm playlist={playlist} onCancel={() => { }} />)
  }

  it('should render empty form', () => {
    setup({})
  })

  it('should render filled form', () => {
    setup({ playlist: mockPlaylists[1] })
    // expect(screen.getByLabelText('Name:')).toHaveValue('')
    expect(screen.getByRole('form', {
      name: 'Playlist edit'
    })).toHaveFormValues({
      name: mockPlaylists[1].name,
      public: mockPlaylists[1].public,
      description: mockPlaylists[1].description,
    })
  })

})