import { render, screen } from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import { mockPlaylists } from '../containers/mockPlaylists'
import { PlaylistsList } from './PlaylistsList'

describe('PlaylistsList', () => {

  const setup = ({
    playlists = mockPlaylists,
    selectedId = ''
  }) => {
    const onSelectSpy = jest.fn()
    render(<PlaylistsList playlists={playlists}
      onSelect={onSelectSpy}
      selectedId={selectedId} />)

    return { onSelectSpy }
  }

  it('should render empty', () => {
    setup({ playlists: [] })

    expect(screen.getByText('No playlists'))
  })

  it('should render empty', () => {
    setup({})

    screen.findAllByRole('listitem', { name: 'playlist-item' })
  })

  it('highlights selected element', () => {
    setup({ selectedId: mockPlaylists[1].id })
    const items = screen.getAllByRole('listitem', { name: 'playlist-item' })

    expect(items[0]).not.toHaveClass('active')
    expect(items[1]).toHaveClass('active')
  })

  it('notifies when element is selected ', () => {
    const { onSelectSpy } = setup({})
    const items = screen.getAllByRole('listitem', { name: 'playlist-item' })
    userEvent.click(items[1])
    expect(onSelectSpy).toHaveBeenCalledWith(mockPlaylists[1].id)

  })

})