import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { PlaylistsList } from './PlaylistsList';
import { mockPlaylists } from '../containers/mockPlaylists';

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Playlists/PlaylistsList',
  component: PlaylistsList,
  // More on argTypes: https://storybook.js.org/docs/react/api/argtypes
  argTypes: {
  },
} as ComponentMeta<typeof PlaylistsList>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: ComponentStory<typeof PlaylistsList> = (args) => <PlaylistsList {...args} />;

export const Primary = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
Primary.args = {
  playlists: mockPlaylists
};

export const Empty = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
Empty.args = {
  playlists: []
};


export const Selected = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
Selected.args = {
  playlists: mockPlaylists,
  selectedId: '123'
};

