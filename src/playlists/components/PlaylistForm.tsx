import { ErrorMessage, Field, FieldArray, Form, Formik, FormikErrors, FormikHelpers, useFormik, useFormikContext, withFormik } from 'formik'
import React from 'react'
import { PagingObject, Playlist, Track } from '../../core/model/Playlist'

interface Props {
  playlist: Playlist
  onCancel(): void
  onSave(draft: Playlist): void
}

export const PlaylistForm = ({
  playlist = {
    id: '', name: '', public: false, description: '', /* extra: { placki: 123 } */
    tracks: { items: [] }
  } as unknown as Playlist,
  onCancel,
  onSave
}: Props) => {

  const submit = (values: Playlist, formikHelpers: FormikHelpers<Playlist>) => {
    const draft: Playlist = { ...playlist, ...values }
    onSave(draft)
  }
  const validate = (values: Playlist): FormikErrors<Playlist> | void => {
    const errors: FormikErrors<Playlist> = {}
    if (values.name?.length <= 3) { errors.name = 'Too Short' }
    if (values.name === '') { errors.name = 'Required' }

    return errors
  }

  return <Formik initialValues={playlist} validate={validate}
    enableReinitialize={true}
    onSubmit={submit}>
    <Form>

      {/* {JSON.stringify(values, null, 2)} */}
      <div className="form-group mb-3">
        <label htmlFor="playlist_name">Name: </label>
        <Field id="playlist_name" className="form-control" name="name" />
        <ErrorMessage name="name" />
      </div>

      <PublicField />

      <div className="form-group mb-3">
        <label htmlFor="playlist_name">Description: </label>
        <Field component="textarea" className="form-control" name="description" />
        <ErrorMessage name="description" />
      </div>
      {/* <div className="form-group mb-3">
        <label htmlFor="playlist_name">Description: </label>
        <Field component="textarea" className="form-control" name="exta.placki" />
        <ErrorMessage name="exta.placki" />
      </div> */}

      <TracksList />

      <button className="btn btn-danger" type="button" onClick={onCancel}>Cancel</button>
      <button className="btn btn-info" type="submit">Save</button>
    </Form>
  </Formik >
}

export const TracksList = () => {
  const { values } = useFormikContext<Playlist>();

  return <FieldArray
    name="tracks.items"
    render={arrayHelpers => (
      <div>
        {values.tracks?.items && values.tracks.items.length > 0 ? (
          values.tracks.items.map((track, index) => (
            <div key={index}>
              <Field name={`items.tracks.${index}.name`} />
              <button
                type="button"
                onClick={() => arrayHelpers.remove(index)} // remove a friend from the list
              > - </button>
              <button
                type="button"
                onClick={() => arrayHelpers.insert(index, { name: '' })} // insert an empty string at a position
              >
                +
              </button>
            </div>
          ))
        ) : (
          <button className="btn btn-info my-3" type="button" onClick={() => arrayHelpers.push('')}>
            {/* show this when user has removed all items.tracks from the list */}
            Add Track
          </button>
        )}
      </div>
    )}
  />

}


export const PublicField = () => <div className="form-group mb-3">
  <label htmlFor="playlist_name">
    <Field type="checkbox" name="public" /> Public </label>
  <ErrorMessage name="public" />
</div>

//   return (
//     <form aria-label="Playlist edit">
//       <div className="form-group  mb-3">
//         <label htmlFor="playlist_name">Name:</label>
//         <input id="playlist_name" type="text" className="form-control" name="name" value={playlist.name} />
//       </div>
//       <div className="form-group  mb-3">
//         <label htmlFor="playlist_public">
//           <input type="checkbox" id="playlist_public" name="public"
//             checked={playlist.public} /> Public </label>
//       </div>
//       <div className="form-group  mb-3">
//         <label htmlFor="">Description</label>
//         <textarea className="form-control" name="description" value={playlist.description} />
//       </div>
//       <button className="btn btn-danger" type="button" onClick={onCancel}>Cancel</button>
//       <button className="btn btn-info">Save</button>
//     </form>
//   )
// }
