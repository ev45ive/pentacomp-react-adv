import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { PlaylistForm } from './PlaylistForm';
import { mockPlaylists } from '../containers/mockPlaylists';

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Playlists/PlaylistForm',
  component: PlaylistForm,
  // More on argTypes: https://storybook.js.org/docs/react/api/argtypes
  argTypes: {
  },
} as ComponentMeta<typeof PlaylistForm>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: ComponentStory<typeof PlaylistForm> = (args) => <PlaylistForm {...args} />;

export const Primary = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
Primary.args = {
  playlist: mockPlaylists[0]
};

export const Empty = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
Empty.args = {
  playlist: undefined
};