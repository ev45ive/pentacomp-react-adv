import { render, screen } from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import { mockPlaylists } from '../containers/mockPlaylists'
import { PlaylistsDetails } from './PlaylistsDetails'

describe('PlaylistsDetails', () => {

  const setup = ({
    playlist = mockPlaylists[0]
  }) => {
    const onEditSpy = jest.fn()
    const { rerender } = render(
      <PlaylistsDetails
        onEdit={onEditSpy}
        playlist={playlist}
      />)
    return { rerender, onEditSpy }
  }

  test('should show playlist details', () => {
    const { rerender } = setup({})

    expect(screen.getByRole('definition', { name: /Name/ }))
      .toHaveTextContent(mockPlaylists[0].name)
    expect(screen.getByRole('definition', { name: /Public/ }))
      .toHaveTextContent('Yes')
    expect(screen.getByRole('definition', { name: /Description/ }))
      .toHaveTextContent(mockPlaylists[0].description)
  })

  test('Playlist public should be Yes', () => {
    const playlist = { ...mockPlaylists[0] }
    
    playlist.public = true
    const { rerender, onEditSpy } = setup({ playlist })
    expect(screen.getByRole('definition', { name: /Public/ }))
      .toHaveTextContent('Yes')

    playlist.public = false
    rerender(<PlaylistsDetails onEdit={onEditSpy} playlist={playlist} />)
    expect(screen.getByRole('definition', { name: /Public/ }))
      .toHaveTextContent('No')

  })

  test('Edit button emits playlist id', () => {
    const { onEditSpy } = setup({})

    const editBtn = screen.getByRole('button', { name: /Edit/ })
    userEvent.click(editBtn)
    expect(onEditSpy).toHaveBeenCalledWith(mockPlaylists[0].id)
  })

})