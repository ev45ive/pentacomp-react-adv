import React from 'react'
import { Playlist } from '../../core/model/Playlist'

interface Props {
  playlist: Playlist,
  onEdit(id: Playlist['id']): void
}

export const PlaylistsDetails = ({ onEdit, playlist }: Props) => {
  return (
    <div>
      {/* dl>(dt{Name:}+dd{Playlist})*2 */}
      <dl>
        <dt id="playlist_name">Name:</dt>
        <dd aria-labelledby="playlist_name">{playlist.name}</dd>

        <dt id="playlist_public">Public:</dt>
        <dd aria-labelledby="playlist_public">{
          playlist.public ? "Yes" : 'No'
        }</dd>

        <dt id="playlist_description">Description:</dt>
        <dd aria-labelledby="playlist_description">{
          playlist.description
        }</dd>
      </dl>
      <button className="btn btn-info" onClick={() => onEdit(playlist.id)}>
        Edit</button>
    </div>
  )
}
