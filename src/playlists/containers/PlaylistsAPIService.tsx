import { Playlist } from "../../core/model/Playlist";

export const fetchPlaylists = (): Promise<Playlist[]> => {
  return fetch('/playlists').then(res => res.json())
};

export const fetchPlaylistById = (id: string): Promise<Playlist> => {
  return fetch('/playlists/' + id).then(res => res.json())
};
