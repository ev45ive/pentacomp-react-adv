import { Playlist } from '../../core/model/Playlist';

export const mockPlaylists = [
  { id: '123', name: "Playlist 123", public: true, description: '' },
  { id: '234', name: "Playlist 234", public: true, description: '' },
  { id: '345', name: "Playlist 345", public: true, description: '' },
] as Playlist[];
