import { fireEvent, logRoles, render, screen, waitFor, waitForElementToBeRemoved, within, } from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import { fetchPlaylists } from './PlaylistsAPIService'
import { PlaylistsView } from './PlaylistsView'
import { mocked } from 'ts-jest/utils'
import { act } from 'react-dom/test-utils'
import { mockPlaylists } from './mockPlaylists'
import { server } from '../../core/mocks/server'

// jest.mock('./PlaylistsAPIService')

describe('PlaylistsView', () => {

  const setup = ({ playlists = mockPlaylists }) => {
    // server.resetHandlers([]) // prepare mocks!!

    // mocked(fetchPlaylists).mockResolvedValue(playlists)
    const { container } = render(<PlaylistsView />)

    // logRoles(container)
  }

  test('shows no playlists', async () => {
    setup({ playlists: [] })

    await waitFor(() =>
      expect(screen.getByText('No playlists'))
    )
    expect(screen.queryByText('Playlist 123')).not.toBeInTheDocument()
    await waitFor(() => { })
  })

  test('shows user playlists', async () => {
    setup({})
    // expect(mocked(fetchPlaylists)).toHaveBeenCalled() // doesnt work with MSW

    await screen.findAllByRole('listitem', { name: 'playlist-item' })
  })

  test('selecting playlist shows playlist details', async () => {
    setup({})
    const details_column = (screen.getByTestId('details_column'))
    const inDetails = within(details_column)

    const items = await screen.findAllByRole('listitem', { name: 'playlist-item' })

    expect(inDetails.queryByText(mockPlaylists[1].name))
      .not.toBeInTheDocument()

    userEvent.click(items[1])

    await inDetails.findByText(mockPlaylists[1].name)
  })


  it('toggle playlist edit form', async () => {
    setup({})

    const details_column = (screen.getByTestId('details_column'))
    const inDetails = within(details_column)

    const items = await screen.findAllByRole('listitem', { name: 'playlist-item' })
    userEvent.click(items[1])

    const btn = await screen.findByRole('button', { name: /Edit/ })

    userEvent.click(btn)

    const form = await screen.findByRole('form', { name: /Playlist edit/ })

    expect(inDetails.queryByText(mockPlaylists[1].name))
      .not.toBeInTheDocument()

    const btnCancel = within(form).getByRole('button', { name: /Cancel/ })
    userEvent.click(btnCancel)

    expect(screen.queryByRole('form', { name: /Playlist edit/ }))
      .not.toBeInTheDocument()

    // await waitForElementToBeRemoved(() => screen.queryByRole('form', {
    //   name: /Playlist edit/
    // }))
  })

})
