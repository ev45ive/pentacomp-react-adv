// tsrafc
import React, { useEffect, useState } from 'react'
import { Playlist } from '../../core/model/Playlist'
import { PlaylistForm } from '../components/PlaylistForm'
import { PlaylistsDetails } from '../components/PlaylistsDetails'
import { PlaylistsList } from '../components/PlaylistsList'
import { fetchPlaylistById, fetchPlaylists } from './PlaylistsAPIService'


export const PlaylistsView = (props: {}) => {

  const [playlists, setPlaylists] = useState<Playlist[]>([])
  const [selectedId, setSelectedId] = useState<string | undefined>(undefined)
  const [selected, setSelected] = useState<Playlist | undefined>()
  const [mode, setMode] = useState<'details' | 'edit'>('details')

  useEffect(() => {
    fetchPlaylists().then(data => setPlaylists(data))
  }, [])

  useEffect(() => {
    if (!selectedId) return;
    // setSelected(playlists.find(p => p.id == selectedId))
    fetchPlaylistById(selectedId).then(data => setSelected(data))
  }, [selectedId])

  const editMode = () => setMode('edit')
  const cancel = () => setMode('details')

  return (
    <div>
      <div className="row">
        <div className="col">
          <div className="display-4 mb-3">Playlists</div>
        </div>
      </div>
      <div className="row">
        <div className="col">
          {/* .list-group>.list-group-item*3{$. Playlist $} */}
          <PlaylistsList playlists={playlists} onSelect={id => setSelectedId(id)}
            selectedId={selectedId} />
        </div>
        <div className="col" data-testid="details_column">

          {selected && mode === 'details' &&
            <PlaylistsDetails onEdit={editMode} playlist={selected} />}

          {selected && mode === 'edit' && <div>
            <PlaylistForm playlist={selected} onCancel={cancel} onSave={() => { }} />
          </div>}
        </div>
      </div>

    </div>
  )
}
