import React from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import { PlaylistsView } from './playlists/containers/PlaylistsView';
import { NavBar } from './core/components/NavBar';

function App() {
  return (
    <div className="container">
      <div className="row">
        <div className="col">
          <NavBar />
          <PlaylistsView />
        </div>
      </div>
    </div>
  );
}

export default App;
