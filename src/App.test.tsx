import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';


jest.mock('./playlists/containers/PlaylistsView', () => ({
  PlaylistsView: () => null
}))

test('renders learn react link', () => {
  render(<App />);
  const linkElement = screen.getByText(/Music App/);
  expect(linkElement).toBeInTheDocument();
});
